# Absibleインストール

## インストール

```bash
[root@localhost ~]# sh ansible-install.sh
```

## インストール後確認

```bash
[root@localhost ~]# rpm -qa | grep ansible
ansible-2.9.9-1.el7.noarch
```
